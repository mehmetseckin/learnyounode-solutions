var fs = require('fs'); // Load the filesystem module.

if(process.argv.length > 2) {
	var buffer = fs.readFileSync(process.argv[2]);
	var content = buffer.toString();
	var lines = content.split('\n');
	var lineCount = lines.length - 1;
	console.log(lineCount);

}
else {
	console.log('Please provide the path to file.');
}
