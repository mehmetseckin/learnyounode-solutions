// Loading modules
var http = require('http');
var fs = require('fs');

// Creating a server.
var server = http.createServer(function(request, response) {
	
	fs.createReadStream(process.argv[3]).on('open', function () { this.pipe(response) });
	
});

server.listen(process.argv[2]);
