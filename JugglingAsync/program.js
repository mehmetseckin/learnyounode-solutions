var http = require('http');
var bl = require('bl');

var urls = process.argv.slice(2);
var responses = [];
for(i=0; i < urls.length; i++) {
	(function(i){
	
		http.get(urls[i], function(response) {
			response.setEncoding('UTF8');
			response.pipe(bl(function (err, data) {
				if(!err) {
					responses[i] = data.toString();
					if(responses.length == urls.length) {
						for(j=0; j < urls.length; j++) {
							console.log(responses[j]);
						}
					}
				}
			}));
		});

	})(i);

}
