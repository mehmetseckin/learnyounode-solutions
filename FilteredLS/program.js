var fs = require('fs');
var path = require('path');

if(process.argv.length > 3) {
	var mPath = process.argv[2];
	var ext = '.' + process.argv[3];
	
	fs.readdir(mPath, function(err, list) {
		if(!err) {
			for(i=0; i<list.length; i++) {
				if(path.extname(list[i]) == ext) {
					console.log(list[i]);
				}
			}
		}
	});

}
