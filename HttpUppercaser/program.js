var http = require('http');

if(process.argv.length > 1) {
	var port = process.argv[2];
	var server = http.createServer(function(req, res) {
		if(req.method == "POST") {
			var body = '';
			req.on('data', function(data) {
				body += data.toString().toUpperCase();
			});
			req.on('end', function() { 
			 res.writeHead('200', {'Content-Type': 'text/html'});
			 res.end(body);
			});
		}
	}).listen(port);
}
else {
	console.log("Please provide a port number.");
}
