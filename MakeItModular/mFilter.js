var fs = require('fs');
var path = require('path');

function mFilter(mPath, ext, callback) {
  fs.readdir(mPath, function(err, list) {
    if(err)  {
      return callback(err, list);
    }

    var filtered = list.filter(function(file) {
        return path.extname(file) === '.' + ext;
    });

    callback(null, filtered);
  });
};


module.exports = mFilter;
