var mFilter = require('./mFilter');

if(process.argv.length > 3) {

  var dirname = process.argv[2];
  var extension = process.argv[3];

  mFilter(dirname, extension, function(err, list) {
    if(err)
      console.log(err);

    for(i=0; i < list.length; i++) {
       console.log(list[i]);
    }
  });
}
